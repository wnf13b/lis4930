#Nick Fennell
#LIS 4930

##Assignment 4

###Requirements

1. Created a Splash Screen
2. Display live RSS Feed
3. Allow users to open link in web browser

###Assignment Screenshots

![img](img/1.png)
![img](img/2.png)
![img](img/3.png)
