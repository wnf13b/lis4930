#Nick Fennell
#LIS 4930

##Assignment 4

###Requirements

1. Created a Splash Screen
2. Use SharedPreferences to store data
3. Calculated remaining mortgage payments

###Assignment Screenshots

![img](img/1.png)
![img](img/2.png)
![img](img/3.png)
