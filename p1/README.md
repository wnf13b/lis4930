# 

## Nick Fennell

### Assignment 1 Requirements:

*Three Parts*

1. Create and Display Launcher Icon In App
2. Create and Display a Splash Screen
3. Use OnClickListener's to Play/Pause Music

#### README.md file should include the following items:

* Screenshot of Non-playing Music
* Screenshot of Playing Music

### Assignment Screenshots:

*Screenshot of Before*:

![before](img/before.png)

*Screenshot of After*:

![after](img/after.png)

