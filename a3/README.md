# 

## Nick Fennell

### Assignment 3 Requirements:

#### README.md file should include the following items:

* Screenshot of app before it populates
* Screenshot of app after it populates

#### Assignment Screenshots:

*Screenshot Before*:

![before](img/before.png)

*Screenshot After*:

![after](img/after.png)
