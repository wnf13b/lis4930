# LIS 4930

## Nick Fennell

### Completed Assignments:

1. [Assignment 1](/a1/README.md "README.md")

    * Install git
    * Install java JDK
    * Run JavaC and MyFirstApp

2. [Assignment 2](/a2/README.md "README.md")

    * This assignment had us create an app that helps split the bill at the end of a meal.
    * Formatted Output for Currency
    * Implemented Drop Down Spinners
    * Returned final result to a TextView

3. [Assignment 3](/a3/README.md "README.md")

    * This assignment had us convert currency from U.S. Dollars to the following:
    > 
    > 1. Euros
    >
    > 2. Pesos
    >
    > 3. Canadian Dollars
    *  Used Radio Buttons and Radio Groups
    *  Returned final result to a TextView

4. [Project 1](/p1/README.md "README.md")

    * Create and Display Launcher Icon
    * Create and Display Splash Screen
    * Play and Pause Music Using OnClickListener

5. [Assignment 4](/a4/README.md "README.md")

    * Create and display a splash screen
    * Create and use SharedPreferences Object
    * Calculate total paid to mortgage

6. [Assignment 5](/a5/README.md "README.md")

    * Create and Display Splash Screen
    * Display live RSS feed from BBC
    * Allow user to open full articles in web browser

7. [Project 2](/p2/README.md "README.md")

    * Create SQLite DB
    * Read and Update from SQLite DB
    * Delete from SQLite DB
    * Display tasks from DB
